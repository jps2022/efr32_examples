/*
 *
 */

#include "init.h"

void gpio_init()
{
  GPIO_PinModeSet(LED_RED_PORT, LED_RED_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(LED_GREEN_PORT,LED_GREEN_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(LED_BLUE_PORT,LED_BLUE_PIN, gpioModePushPull, 1);
}
