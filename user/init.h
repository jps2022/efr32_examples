/*
 *
 */

#ifndef INIT_H_
#define INIT_H_

#include "bsp.h"
#include <stdint.h>

#include "em_emu.h"
#include "em_cmu.h"
//#include "em_cryotimer.h"
//#include "em_timer.h"
//#include "em_usart.h"
//#include "em_adc.h"

#define LED_RED_PORT		gpioPortD
#define LED_RED_PIN		13

#define LED_BLUE_PORT		gpioPortC
#define LED_BLUE_PIN		11

#define LED_GREEN_PORT		gpioPortC
#define LED_GREEN_PIN		10

void gpio_init();

#endif
