/*
 * usart.c
 *
 */

#include "usart.h"

unsigned char ct_char;
uint8_t received_usart_string[255];
unsigned char send_string_bluetooth;

/* Define termination character */
#define TERMINATION_CHAR    '.'

/* Declare a circular buffer structure to use for Rx and Tx queues */
#define BUFFERSIZE      256
static volatile int     rxWriteIndex = 0;       /**< Index in buffer to be written to */
static volatile int     rxCount      = 0;       /**< Keeps track of how much data which are stored in the buffer */
static uint8_t 		rxBuffer[BUFFERSIZE];    /**< Buffer to store data */
uint8_t 		counter_baudrate_detection = 0;

static USART_TypeDef *uart = USART1;

void Init_Usart1 (uint32_t baudrate)
{
  /* Enable clock for GPIO module (required for pin configuration) */
  CMU_ClockEnable (cmuClock_GPIO, true);
  CMU_ClockEnable (cmuClock_USART1, true);
  /* Configure GPIO pins */
//  GPIO_PinModeSet (gpioPortA, 0, gpioModePushPull, 1);
  GPIO_PinModeSet (gpioPortA, 1, gpioModeInputPull, 1);

  USART_InitAsync_TypeDef uartInit = USART_INITASYNC_DEFAULT;
  /* Prepare struct for initializing UART in asynchronous mode*/
  uartInit.enable = usartEnableRx; 			/* Don't enable UART upon intialization */
  uartInit.refFreq = 0; 				/* Provide information on reference frequency. When set to 0, the reference frequency is */
  uartInit.baudrate = baudrate; 			/* Baud rate */
  uartInit.oversampling = usartOVS16; 			/* Oversampling. Range is 4x, 6x, 8x or 16x */
  uartInit.databits = usartDatabits8; 			/* Number of data bits. Range is 4 to 10 */
  uartInit.parity = usartNoParity; 			/* Parity mode */
  uartInit.stopbits = usartStopbits1; 			/* Number of stop bits. Range is 0 to 2 */
  uartInit.mvdis = false; 				/* Disable majority voting */
  uartInit.prsRxEnable = false; 			/* Enable USART Rx via Peripheral Reflex System */
  uartInit.prsRxCh = usartPrsRxCh0; 			/* Select PRS channel if enabled */

  /* Initialize USART with uartInit struct */
  USART_InitAsync (uart, &uartInit);

  /* Prepare UART Rx and Tx interrupts */
  USART_IntClear(uart, _USART_IFC_MASK);
  USART_IntEnable (uart, USART_IEN_RXDATAV);
  NVIC_EnableIRQ (USART1_RX_IRQn);
//  NVIC_EnableIRQ (USART1_TX_IRQn);
  NVIC_ClearPendingIRQ (USART1_RX_IRQn);
//  NVIC_ClearPendingIRQ (USART1_TX_IRQn);

  uart->ROUTEPEN = USART_ROUTEPEN_RXPEN;
//  uart->ROUTEPEN = USART_ROUTEPEN_TXPEN;
  uart->ROUTELOC0 = USART_ROUTELOC0_RXLOC_LOC0;
//  uart->ROUTELOC0 = USART_ROUTELOC0_TXLOC_LOC0;

//  uart->ROUTEPEN = USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_TXPEN;
//  uart->ROUTELOC0 = (uart->ROUTELOC0
//		      & ~(_USART_ROUTELOC0_TXLOC_MASK
//			  | _USART_ROUTELOC0_RXLOC_MASK) )
//		     | (USART_ROUTELOC0_TXLOC_LOC6)
//		     | (USART_ROUTELOC0_RXLOC_LOC6);
  /* Enable UART */
  USART_Enable (uart, usartEnable);
}


/**************************************************************************//**
 * @brief UART1 RX IRQ Handler
 *
 * Set up the interrupt prior to use
 *
 * Note that this function handles overflows in a very simple way.
 *
 *****************************************************************************/
void USART1_RX_IRQHandler (void)
{
  uint8_t rxData = USART_Rx(uart);
  if ((rxData != '\r') && (rxData != '\n'))      //terminação da linha para tratar a string
  {
    rxBuffer[rxCount++] = rxData; 		 //Enquanto for diferente do \r e \n vai jogando os bytes no vetor
  }
  else if (rxData == '\n')
  {
    rxBuffer[rxCount] = '\0';
    rxCount = 0;
    printf("%s\r\n", rxBuffer);
  }
}

//void USART1_STR (uint8_t *message, uint16_t size)         //Escreve uma string via RS-232
//{
//  uint8_t *ch = message;
//
//  for(uint16_t current_byte = 0; current_byte < size; current_byte++)
//  {
//    USART_Tx (USART1, (uint8_t) ch[current_byte]); //  Put the next character into the data transmission
//  }
//
//}

