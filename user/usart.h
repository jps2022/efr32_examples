/*
 * usart.h
 *
 */

#ifndef USART_H_
#define USART_H_

#include "string.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_cryotimer.h"
#include "em_timer.h"
#include "em_usart.h"
#include "init.h"
#include "bsp.h"
#include <stdint.h>
#include <stdio.h>

extern unsigned char ct_char;
extern uint8_t received_usart_string[255];
extern unsigned char send_string_bluetooth;

void Init_Usart1 (uint32_t baudrate);
//void USART1_STR (uint8_t *message, uint16_t size);

#endif /* USART_H_ */
